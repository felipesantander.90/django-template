![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54)
![Django](https://img.shields.io/badge/django-%23092E20.svg?style=for-the-badge&logo=django&logoColor=white)
![SQLite](https://img.shields.io/badge/sqlite-%2307405e.svg?style=for-the-badge&logo=sqlite&logoColor=white)
![Swagger](https://img.shields.io/badge/-Swagger-%23Clojure?style=for-the-badge&logo=swagger&logoColor=white)

[![Maintenance](https://img.shields.io/badge/Maintained%3F-yes-green.svg)](https://GitHub.com/Naereen/StrapDown.js/graphs/commit-activity)
[![Docker](https://badgen.net/badge/icon/docker?icon=docker&label)](https://https://docker.com/)


# django template

this is my personal template for api on django restaframework

## this projects have

- schema validation entry key with ceberus
- test
- ci/cd with aws lightsail
- deploy local and server with unicorn
- logs at json format
- app test (sum)
- deploy on docker
- file env example
- bash for deploy django with one user admin

## Getting started

first create a fork from this repo.

take a time for replace the name of the author.
the file for this is:

``` src.services.main.urls.py
SchemaView = get_schema_view(
   openapi.Info(
      title="Simple Math API",
      default_version='v2',
      description="Esta es una API para el suma de dos numeros",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="felipesantande.90@gmail.com"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=[permissions.AllowAny],
)
```


take a time for clone the file .env copy and rename to .env in this file will there are all the env need 

```
LOG_LEVEL=DEBUG
SECRET_KEY=django-insecure-o24d58n8i8sou%83(!zew-_^%)i^$k9lv4t6n5#wx2je5k@xr!
RPC_SERVICE_1=rpc
APP_NAME=template1
PORT=8004
URL_SERVER_BACKEND=http://localhost:8004
USER_ADMIN_DJANGO=admin
PASSWORD_ADMIN_DJANGO=admin
PROJECT_NAME=template
```

## Build:
```
docker-compose -f docker-compose-local.yml build
```

## Start:

check if is totally ok, with:

```
docker-compose -f docker-compose-local.yml up
```

in your console will be something like this:

```
template1  | ModuleNotFoundError: No module named 'django_q'
template1  | /app/template/services/math/views/sum.py changed, reloading.
template1  | {"message": "/app/template/services/math/views/sum.py changed, reloading."}
template1  | {"message": "Register RPC method \"test\""}
template1  | {"message": "Method registered. len(registry): 1"}
template1  | {"message": "Register RPC method \"system.listMethods\""}
template1  | {"message": "Method registered. len(registry): 2"}
template1  | {"message": "Register RPC method \"system.methodHelp\""}
template1  | {"message": "Method registered. len(registry): 3"}
template1  | {"message": "Register RPC method \"system.methodSignature\""}
template1  | {"message": "Method registered. len(registry): 4"}
template1  | {"message": "Register RPC method \"system.multicall\""}
template1  | {"message": "Method registered. len(registry): 5"}
template1  | {"message": "django-modern-rpc initialized: 5 RPC methods registered"}
template1  | Watching for file changes with StatReloader
template1  | {"message": "Watching for file changes with StatReloader"}
template1  | Performing system checks...
template1  | 
template1  | System check identified no issues (0 silenced).
template1  | 
template1  | You have 1 unapplied migration(s). Your project may not work properly until you apply the migrations for app(s): math.
template1  | Run 'python manage.py migrate' to apply them.
template1  | March 24, 2023 - 19:38:33
template1  | Django version 4.0.3, using settings 'services.main.settings'
template1  | Starting development server at http://0.0.0.0:8000/
template1  | Quit the server with CONTROL-C.
```

## Test:

for test you can use the extension for vscode REST Client thunder client, postman or curl

```
curl -X POST \
  'http://localhost:8004/sum_example/sum' \
  --header 'Accept: */*' \
  --header 'User-Agent: Thunder Client (https://www.thunderclient.com)' \
  --header 'Content-Type: application/json' \
  --data-raw '{
  "first_number": 2,
  "second_number": 3
}'
```

## Deploy:

Now you can deploy with docker-compose

```
docker-compose -f docker-compose.yml up -d
```

if you want to develop directly on the proyect you can install the requirements.txt

```
pip install -r requirements.txt
```

i recommend use the extension for vscode remote container, this will help you to develop in a container


## Testings

execute the test with:

```
docker-compose -f docker-compose-local.yml exec django python src/manage.py test src
```

result:

```
WARN[0000] The "k9lv4t6n5" variable is not set. Defaulting to a blank string. 
{"message": "Register RPC method \"test\""}
{"message": "Method registered. len(registry): 1"}
{"message": "Register RPC method \"system.listMethods\""}
{"message": "Method registered. len(registry): 2"}
{"message": "Register RPC method \"system.methodHelp\""}
{"message": "Method registered. len(registry): 3"}
{"message": "Register RPC method \"system.methodSignature\""}
{"message": "Method registered. len(registry): 4"}
{"message": "Register RPC method \"system.multicall\""}
{"message": "Method registered. len(registry): 5"}
{"message": "django-modern-rpc initialized: 5 RPC methods registered"}
Found 1 test(s).
Creating test database for alias 'default'...
System check identified no issues (0 silenced).
/sum_example/sum
21:44:00 [Q] INFO Enqueued 1
{"message": "sum_success"}
.
----------------------------------------------------------------------
Ran 1 test in 0.019s

OK
Destroying test database for alias 'default'...
```

## License
For open source projects, say how it is licensed.



