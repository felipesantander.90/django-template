from drf_yasg import openapi

schema_error_200 = openapi.Schema(
      '200',
      type=openapi.TYPE_OBJECT,
      description='Success response',
      properties={
          'result': openapi.Schema(
              type=openapi.TYPE_INTEGER, description='total sum'
            ),
        },
      required=['detail']
)

response_sum_200 = openapi.Response(
    'total sum',
    schema_error_200
)

responses_user_profile = {
    200: response_sum_200,
}
