"""errors for API mobile service
"""
from rest_framework.response import Response
cerberus_error_response = lambda x: Response(x, status=400)