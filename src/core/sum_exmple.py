def sum_to_numbers(first_number: int , second_number: int) -> int:
    """
    This function is for sum two numbers
    """
    return first_number + second_number
