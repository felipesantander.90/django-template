from modernrpc.core import rpc_method
from modernrpc.auth.basic import http_basic_auth_login_required, http_basic_auth_superuser_required, \
     http_basic_auth_permissions_required, http_basic_auth_any_of_permissions_required, \
     http_basic_auth_group_member_required, http_basic_auth_all_groups_member_required
from modernrpc.core import rpc_method

@rpc_method
@http_basic_auth_login_required
def test():
    return True