# pylint: disable=missing-class-docstring
from django.apps import AppConfig


class MathConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'services.math'
    verbose_name = 'math'
