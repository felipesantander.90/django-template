"""urls for red"""
from django.urls import path
from . import views

urlpatterns = [
    path('sum', views.endpoint_sum, name='sum'),
]
