import json
from django.urls import reverse
from rest_framework.test import APITestCase
from rest_framework import status
from services.math.models import SumExample

# Create your tests here.
class SumExampleTest(APITestCase):
    """test for all endpoint on example"""

    def setUp(self):
        pass

    def test_sum(self):
        """test sum endpoint"""
        first_number = 1
        second_number = 2
        url = reverse('sum')
        response = self.client.post(
            url,
            data=json.dumps({'first_number': first_number, 'second_number': second_number}),
            content_type='application/json'
        )
        json_response = response.json()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json_response['result'], first_number + second_number)
        self.assertEqual(SumExample.objects.count(), 1)
