# Generated by Django 4.0.3 on 2023-04-18 20:47

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('math', '0002_multiexample_and_more'),
    ]

    operations = [
        migrations.DeleteModel(
            name='MultiExample',
        ),
    ]
