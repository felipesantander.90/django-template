import logging
from rest_framework.decorators import api_view
from rest_framework.response import Response
from cerberus import Validator
from drf_yasg.utils import swagger_auto_schema
from schema.sum import sum_schemas
from core.sum_exmple import sum_to_numbers
from core.errors import cerberus_error_response
from swagger.services.math.views import responses_user_profile
from services.math.models import SumExample


# Create your views here.
logger = logging.getLogger(__name__)


@swagger_auto_schema(method='post', manual_parameters=[], responses=responses_user_profile)
@api_view(['POST'])
def endpoint_sum(request):
    """ this method is for sum two numbers
    Args:
        request (any): request object
        request.data (dict): data of request
        request.data.first_number (int): first_number number
        request.data.second_number (int): second number
        

    Returns:
        object : return the sum of two numbers
    """
    log_prefix = "sum"
    validator = Validator(sum_schemas.sum_schema)
    if validator.validate(request.data) is False:
        logger.error('%s_validation_error %s', log_prefix, validator.errors)
        return cerberus_error_response(validator.errors)
    first_number = request.data['first_number']
    second_number = request.data['second_number']
    result = sum_to_numbers(first_number, second_number)
    SumExample(first_number=first_number, second_number=second_number, result=result).save()
    logger.info('%s_success', log_prefix)
    return Response({'result': result})
