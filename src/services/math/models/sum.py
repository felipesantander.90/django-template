from django.db import models

# generate docs models
class SumExample(models.Model):
    """model for sum example

    Args:
        first_number (int): first number
        second_number (int): second number
        result (int): result of sum
    """
    first_number = models.IntegerField()
    second_number = models.IntegerField()
    result = models.IntegerField()

    def __str__(self):
        return str(self.first_number) + ' + ' + str(self.second_number) + ' = ' + str(self.result)

    def save(self, *args, **kwargs):
        self.result = self.first_number + self.second_number
        super(SumExample, self).save(*args, **kwargs)

    class Meta:
        db_table = 'sum_example'
        verbose_name = 'Sum Example'
        verbose_name_plural = 'Sum Examples'
        ordering = ['first_number', 'second_number']
        unique_together = ('first_number', 'second_number')
        indexes = [
            models.Index(fields=['first_number', 'second_number']),
        ]
        permissions = (
            ('view_sum_example', 'Can view Sum Example'),
        )
