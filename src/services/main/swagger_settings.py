from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions
from django.conf import settings


SchemaView = get_schema_view(
   openapi.Info(
      title=settings.SWAGGER_APP_TITLE,
      default_version=settings.SWAGGER_APP_VERSION,
      description=settings.SWAGGER_APP_DESCRIPTION,
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email=settings.SWAGGER_APP_CONTACT),
      license=openapi.License(name=settings.SWAGGER_APP_LICENSE),
   ),
   public=True,
   permission_classes=[permissions.AllowAny],
)
