#!/bin/sh
echo VERSION=$(curl --request GET --header "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" 'https://gitlab.com/api/v4/projects/$ID_GITLAB_PROJECT/repository/tags' | jq -r '.[0] | .name')>> .env
echo DOCKER_REGISTRY=$DOCKER_REGISTRY   >> .env
echo APP_NAME=$APP_NAME   >> .env
echo CI_PIPELINE_IID=$CI_PIPELINE_IID   >> .env
echo SECRET_KEY=$SECRET_KEY >> .env
echo RPC_SERVICE_1=$RPC_SERVICE_1 >> .env
echo PORT=$PORT >> .env
echo LOG_LEVEL=$LOG_LEVEL >> .env
echo LOKI_SERVER=$LOKI_SERVER >> .env